/*
 * wifi.h
 *
 *  Created on: 1 sep. 2021
 *      Author: ischneider
 */

#ifndef WIFI_INC_WIFI_H_
#define WIFI_INC_WIFI_H_

#include "esp_event.h"

typedef enum rv_wifi_t_{
	WIFI_OK,
	WIFI_ERROR,
	WIFI_CONNECTED,
	WIFI_GOT_IP
}rv_wifi_t;

void on_got_ip(void *arg, esp_event_base_t event_base,int32_t event_id, void *event_data);
rv_wifi_t wifi_got_ip(uint32_t ticks);
void wifi_init();
void wifi_delete();
rv_wifi_t wifi_connect(char* ssid, char* pass);

#endif /* WIFI_INC_WIFI_H_ */
