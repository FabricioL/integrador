/*
 * wifi.c
 *
 *  Created on: 1 sep. 2021
 *      Author: ischneider
 */

#include <string.h>
#include "../inc/wifi.h"
#include "esp_log.h"
#include "esp_wifi.h"
#include "nvs_flash.h"

static const char *TAG = "Wifi";
static xSemaphoreHandle s_semph_get_ip_addrs;

esp_err_t event_handler(void *ctx, system_event_t *event)
{
    return ESP_OK;
}

void on_got_ip(void *arg, esp_event_base_t event_base,int32_t event_id, void *event_data)
{
	ip_event_got_ip_t *event = (ip_event_got_ip_t *)event_data;
	ESP_LOGI(TAG, "Got IPv4 event: Interface \"%s\" address: " IPSTR, esp_netif_get_desc(event->esp_netif), IP2STR(&event->ip_info.ip));
	//memcpy(&s_ip_addr, &event->ip_info.ip, sizeof(s_ip_addr));
	xSemaphoreGive(s_semph_get_ip_addrs);
}

void on_lost_ip(void *arg, esp_event_base_t event_base,int32_t event_id, void *event_data)
{
	xSemaphoreTake(s_semph_get_ip_addrs, 1000);
}

rv_wifi_t wifi_got_ip(uint32_t ticks)
{
	if (pdTRUE == xSemaphoreTake(s_semph_get_ip_addrs, ticks))
	{
		xSemaphoreGive(s_semph_get_ip_addrs);
		return WIFI_GOT_IP;
	}
	return WIFI_ERROR;
}

void wifi_init()
{
	s_semph_get_ip_addrs = xSemaphoreCreateBinary();
}

/* Delete WiFi connection */
void wifi_delete()
{
	esp_wifi_disconnect();
	esp_wifi_stop();
	vSemaphoreDelete(s_semph_get_ip_addrs);
}

/* Start WiFi connection */
rv_wifi_t wifi_connect(char* ssid, char* pass)
{
	wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
	//wifi_config_t sta_config;

	// Init NVS partition
	nvs_flash_init();
	// Enable TCP
	tcpip_adapter_init();

	esp_err_t ret;

	// Init event loops
	ret = esp_event_loop_init(event_handler, NULL);
	if (ret != ESP_OK)
	{
		ESP_LOGI(TAG, "error activando eventos");
		// TODO: error check
	}

	// Init WiFi module with default configurations
	ret = esp_wifi_init(&cfg);
	if (ret != ESP_OK)
	{
		ESP_LOGI(TAG, "error iniciando el modulo wifi");
		// TODO: error check
	}

	// Set default wifi event handlers for STA interface
	ret = esp_wifi_set_default_wifi_sta_handlers();
	if (ret != ESP_OK)
	{
		ESP_LOGI(TAG, "error configurando los handlers genericos");
		// TODO: error check
	}

	// Register an event handler to the system event loop
	ret = esp_event_handler_register(IP_EVENT, IP_EVENT_STA_GOT_IP, &on_got_ip, NULL);
	if (ret != ESP_OK)
	{
		ESP_LOGI(TAG, "error configurando el handler de got ip");
		// TODO: error check
	}
	// cambio el handler del evento IP_EVENT_STA_LOST_IP para notificar que no tengo ip
		ret = esp_event_handler_register(IP_EVENT, IP_EVENT_STA_LOST_IP, &on_got_ip, NULL);
		if (ret != ESP_OK)
		{
			ESP_LOGI(TAG, "error configurando el handler de lost ip");
			//todo check error
		}

	// Set storage type
	ret = esp_wifi_set_storage(WIFI_STORAGE_RAM);
	if (ret != ESP_OK)
	{
		ESP_LOGI(TAG, "error configurando el almacenamiento");
		// TODO: error check
	}

	// Set the WiFi operating mode in STA
	ret = esp_wifi_set_mode(WIFI_MODE_STA);
	if (ret != ESP_OK)
	{
		ESP_LOGI(TAG, "error activando el modo STA");
		// TODO: error check
	}
	wifi_config_t sta_config = {
	        .sta = {
	            .ssid = CONFIG_ESP_WIFI_SSID,
	            .password = CONFIG_ESP_WIFI_PASSWORD,
	            .bssid_set = false
	        }
	    };

	/*strcpy((char*)sta_config.sta.ssid,CONFIG_ESP_WIFI_SSID);
	strcpy((char*)sta_config.sta.password,CONFIG_ESP_WIFI_PASSWORD);
	sta_config.sta.bssid_set = false;*/

	// Set the configuration of the ESP32 STA
	ret = esp_wifi_set_config(WIFI_IF_STA, &sta_config);
	if (ret != ESP_OK)
	{
		ESP_LOGI(TAG, "error configurando el modulo wifi");
		// TODO: error check
	}

	// Start WiFi according to current configuration
	ret = esp_wifi_start();
	if(ret != ESP_OK)
	{
		ESP_LOGI(TAG, "error prendiendo el modulo");
		// TODO: error check
	}

	// Connect the ESP32 WiFi station to the AP
	ret = esp_wifi_connect();
	if (ret != ESP_OK)
	{
		ESP_LOGI(TAG, "error conectando el modulo");
		// TODO: error check
	}

	return WIFI_OK;
}
