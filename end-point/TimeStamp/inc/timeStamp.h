/*
 * time.h
 *
 *  Created on: 31 ago. 2021
 *      Author: ischneider
 */

#ifndef TIME_INC_TIME_H_
#define TIME_INC_TIME_H_

#define TM_TO_YEAR(a) a.tm_year+1900
#define TM_TO_MONTH(a) a.tm_mon+1
#define TM_TO_DAY(a) a.tm_mday

#define TM_TO_HOUR(a) a.tm_hour
#define TM_TO_MINUTE(a) a.tm_min
#define TM_TO_SECOND(a) a.tm_sec

void obtain_time(void);
void initialize_sntp(void);
void obtain_time(void);


#endif /* TIME_INC_TIME_H_ */
