/*
 * test_Temperature.c
 *
 *  Created on: 2 sep. 2021
 *      Author: ischneider
 */
#define TEST

#include "esp_sntp.h"
#include "esp_adc_cal.h"
#include "../inc/temperature.h"
#include <stdio.h>
#include <time.h>
#include "test_utils.h"



TEST_CASE("test test", "[temperature]")
{
    int adc1_val[ADC1_TEST_CHANNEL_NUM] = {0};

    /* adc1 Configure */
    adc1_config_width(ADC1_TEST_WIDTH);
    ESP_LOGI(TAG, "ADC1 [CH - GPIO]:");
    for (int i = 0; i < ADC1_TEST_CHANNEL_NUM; i++) {
        TEST_ESP_OK( adc1_config_channel_atten(adc1_ch[i], ADC1_TEST_ATTEN) );
        ESP_LOGI(TAG, "[CH%d - IO%d]:", adc1_ch[i], ADC_GET_IO_NUM(0, adc1_ch[i]));
    }
    printf("ADC tie normal read: ");
    vTaskDelay(10 / portTICK_RATE_MS);

    /* adc Read */
    printf("ADC1: ");
    for (int i = 0; i < ADC1_TEST_CHANNEL_NUM; i++) {
        adc1_val[i] = adc1_get_raw((adc1_channel_t)adc1_ch[i]);
        printf("CH%d-%d ", adc1_ch[i], adc1_val[i]);
    }
    printf("\n");

    /* tie high */
    for (int i = 0; i < ADC1_TEST_CHANNEL_NUM; i++) {
        adc_fake_tie_high(ADC_UNIT_1, adc1_ch[i]);
    }
    printf("ADC tie high read: ");
    vTaskDelay(50 / portTICK_RATE_MS);
    /* adc Read */
    printf("ADC1: ");
    for (int i = 0; i < ADC1_TEST_CHANNEL_NUM; i++) {
        adc1_val[i] = adc1_get_raw((adc1_channel_t)adc1_ch[i]);
        printf("CH%d-%d ", adc1_ch[i], adc1_val[i]);
#ifdef CONFIG_IDF_TARGET_ESP32S2
        TEST_ASSERT_EQUAL( adc1_val[i], 0x1fff );
#endif
    }
    printf("\n");

    /* tie low */
    for (int i = 0; i < ADC1_TEST_CHANNEL_NUM; i++) {
        adc_fake_tie_low(ADC_UNIT_1, adc1_ch[i]);
    }
    printf("ADC tie low  read: ");
    vTaskDelay(50 / portTICK_RATE_MS);
    /* adc Read */
    printf("ADC1: ");
    for (int i = 0; i < ADC1_TEST_CHANNEL_NUM; i++) {
        adc1_val[i] = adc1_get_raw((adc1_channel_t)adc1_ch[i]);
        printf("CH%d-%d ", adc1_ch[i], adc1_val[i]);
#ifdef CONFIG_IDF_TARGET_ESP32S2
        TEST_ASSERT_INT_WITHIN( 100, 0, adc1_val[i] );
#endif
    }
    printf("\n");

    /* tie midedle */
    for (int i = 0; i < ADC1_TEST_CHANNEL_NUM; i++) {
        adc_fake_tie_middle(ADC_UNIT_1, adc1_ch[i]);
    }
    printf("ADC tie mid  read: ");
    vTaskDelay(50 / portTICK_RATE_MS);
    /* adc Read */
    printf("ADC1: ");
    for (int i = 0; i < ADC1_TEST_CHANNEL_NUM; i++) {
        adc1_val[i] = adc1_get_raw((adc1_channel_t)adc1_ch[i]);
        printf("CH%d-%d ", adc1_ch[i], adc1_val[i]);
#ifdef CONFIG_IDF_TARGET_ESP32S2
        TEST_ASSERT_NOT_EQUAL( adc1_val[i], 0x1fff );
        TEST_ASSERT_NOT_EQUAL( adc1_val[i], 0 );
#endif
    }
    printf("\n");

    for (int i = 0; i < ADC1_TEST_CHANNEL_NUM; i++) {
        adc_io_normal(ADC_UNIT_1, adc1_ch[i]);
    }
}
