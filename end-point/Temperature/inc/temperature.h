/*
 * temperature.h
 *
 *  Created on: 31 ago. 2021
 *      Author: ischneider
 */

#ifndef TEMPERATURE_INC_TEMPERATURE_H_
#define TEMPERATURE_INC_TEMPERATURE_H_

#include <stdio.h>
#include <time.h>
#include "esp_adc_cal.h"

// TODO: get the VREF value from a pin
#define DEFAULT_VREF    1138

#define ADC_ID			ADC_UNIT_1
#define ADC_CHANNEL		ADC1_CHANNEL_0
#define ADC_PRECISION	ADC_WIDTH_BIT_12
#define ADC_ATTENUATION	ADC_ATTEN_DB_0

#define SAMPLES_QUANTITY	100

xQueueHandle QDatos;
esp_adc_cal_characteristics_t adc_chars;

typedef struct data_t_{
	uint16_t id;
	uint32_t temp;
	struct tm timeStamp;
}data_t;

void readTemp(void *pvParameter);
void initialize_adc_1(void);


#endif /* TEMPERATURE_INC_TEMPERATURE_H_ */
