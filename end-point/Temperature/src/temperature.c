/*
 * temperature.c
 *
 *  Created on: 31 ago. 2021
 *      Author: ischneider
 */
#include "esp_sntp.h"
#include <driver/adc.h>
#include "../inc/temperature.h"
#include <stdio.h>
#include <time.h>

/* ADC1 init */
// TODO: function to initialize ADC2
void initialize_adc_1(void)
{
	// configure the ADC precision
	adc1_config_width(ADC_PRECISION);
	// configure the ADC attenuation
	adc1_config_channel_atten(ADC_CHANNEL, ADC_ATTENUATION);
	// characterize the ADC
	esp_adc_cal_characterize(ADC_ID, ADC_ATTENUATION, ADC_PRECISION, DEFAULT_VREF, &adc_chars);
}

/* Read temperature task */
// TODO: make another function to send the data
void readTemp(void *pvParameter)
{
	time_t now;
	struct tm timeinfo;
	data_t data;
	uint32_t samples = 0;
	uint32_t average = 0;

	// Init ADC 1
	initialize_adc_1();

	QDatos = xQueueCreate(10,sizeof(data_t));

	while (true)
	{
		samples=0;
		for(int i=0; i<SAMPLES_QUANTITY;i++)
		{
			samples +=  adc1_get_raw(ADC_CHANNEL);
			vTaskDelay(10 / portTICK_PERIOD_MS);
		}

		// get the samples average value
		average = samples / SAMPLES_QUANTITY;
	    data.id = 123;
	    data.temp = esp_adc_cal_raw_to_voltage(average, &adc_chars);
	    // get actual time to generate the timestamp
	    time(&now);
	    localtime_r(&now, &timeinfo);
	    data.timeStamp = timeinfo;
	    // send data to the queue
		xQueueSendToFront(QDatos,&data,100/portTICK_RATE_MS);
		printf("valor adc %.1f\n",(float)data.temp/10);
	}
}
