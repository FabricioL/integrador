/*
 * reportTask.c
 *
 *  Created on: 1 sep. 2021
 *      Author: ischneider
 */
#include "esp_log.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"

#include "mqtt_client.h"
#include "../../Temperature/inc/temperature.h"
#include "../../TimeStamp/inc/timeStamp.h"
#include "../../Wifi/inc/wifi.h"
#include "../../Mqtt/inc/mqtt.h"
#include "../inc/reportTask.h"

static const char *TAG = "Report_task";

void report_initConnections()
{
#if MQTT_OVER == WIFI
	wifi_init();
	wifi_connect(WIFI_SSID,WIFI_PASS);
#else if MQTT_OVER == ETH
	//todo: mqtt por ethernet
#endif
}

void report_deinitConnections()
{
#if MQTT_OVER == WIFI
	wifi_delete();
#else if MQTT_OVER == ETH
	//todo: mqtt por ethernet
#endif
}

void report_init()
{
	BaseType_t stat;

	stat = xTaskCreate(sendData, "sendData", configMINIMAL_STACK_SIZE*5, NULL, 1, &reportTaskHandle);
	if (stat != pdPASS)
	{
		printf("cannot create sendData task\n");
	}
}

void report_delete()
{
#if MQTT_OVER == WIFI
	wifi_delete();
#else if MQTT_OVER == ETH
	//todo: mqtt por ethernet
#endif
	vTaskDelete( reportTaskHandle );
}

uint8_t report_got_ip()
{
#if MQTT_OVER == WIFI
	if (WIFI_GOT_IP == wifi_got_ip(10000/portTICK_PERIOD_MS))
	{
		return true;
	}
	return false;
#else if MQTT_OVER == ETH
	//todo: mqtt por ethernet
#endif
}
typedef enum sendDataStates_t_{
	SEND_DATA_CONNECT,
	SEND_DATA_TIME,
	SEND_DATA_MQTT,
	SEND_DATA_PUBLISH
}sendDataStates_t;

sendDataStates_t sendDataStates=SEND_DATA_CONNECT;

void sendData(void *pvParameter)
{
	data_t dato;
	while(1)
	{
		switch(sendDataStates)
		{
		case SEND_DATA_CONNECT:
			report_initConnections();
			sendDataStates = SEND_DATA_TIME;
			break;
		case SEND_DATA_TIME:
			if (report_got_ip())
			{
				time_t now;
				struct tm timeinfo;
				time(&now);
				localtime_r(&now, &timeinfo);
				// Is time set? If not, tm_year will be (1970 - 1900).
				while (timeinfo.tm_year < (2016 - 1900))
				{
					ESP_LOGI(TAG, "Time is not set yet. Getting time over NTP.");
					obtain_time();
					time(&now);
					localtime_r(&now, &timeinfo);
				}
				sendDataStates = SEND_DATA_MQTT;
			}
			else
			{
				report_deinitConnections();
				sendDataStates = SEND_DATA_CONNECT;
			}
			break;
		case SEND_DATA_MQTT:
			if (report_got_ip())
			{
				mqtt_app_start();
				sendDataStates = SEND_DATA_PUBLISH;
			}
			else
			{
				report_deinitConnections();
				sendDataStates = SEND_DATA_CONNECT;
			}
			break;
		case SEND_DATA_PUBLISH:
			if (xQueuePeek(QDatos, &dato, portMAX_DELAY))
			{
				if (pdTRUE == xSemaphoreTake(s_semph_get_mqtt_broker, (10000/portTICK_PERIOD_MS)))
				{
					xSemaphoreGive(s_semph_get_mqtt_broker);
					char buffer[150];
					char topic[20];
					uint8_t length = 0;
					sprintf(topic, "sensores/nodo_%u",dato.id);
					//strftime(strftime_buf, sizeof(strftime_buf), "%c", &(dato.timeStamp.));
					length = sprintf(buffer,"{\"Id\": %u,\"temp\": %.1f, \"timestamp\": \"%u/%u/%u %u:%u:%u \"}",dato.id, (float)dato.temp/10, TM_TO_DAY(dato.timeStamp), TM_TO_MONTH(dato.timeStamp), TM_TO_YEAR(dato.timeStamp),TM_TO_HOUR(dato.timeStamp),TM_TO_MINUTE(dato.timeStamp),TM_TO_SECOND(dato.timeStamp));
					printf("enviando %s %s\n",topic ,buffer);
					if (!mqtt_publish(topic, buffer, length))// si se pudo enviar saco el dato de la cola
					{
						printf("se envio el dato\n");
						xQueueReceive(QDatos, &dato,0);
					}
				}
				else
				{
					mqtt_app_end();
					report_deinitConnections();
					sendDataStates = SEND_DATA_CONNECT;
				}
			}
			break;
		}
	}
}
