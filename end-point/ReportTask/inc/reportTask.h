/*
 * reportTask.h
 *
 *  Created on: 1 sep. 2021
 *      Author: ischneider
 */

#ifndef REPORTTASK_INC_REPORTTASK_H_
#define REPORTTASK_INC_REPORTTASK_H_

#define MQTT_OVER WIFI // define para conectar mqtt por WIFI o ETH
#define WIFI_SSID "Wifi_lu_ivan"
#define WIFI_PASS "lucianaivan"

typedef struct report_t_{
	char *data;
	uint32_t size;
}report_t;

TaskHandle_t reportTaskHandle;

void report_initConnections();
void report_init();
void report_delete();
uint8_t report_got_ip();
void sendData(void *pvParameter);

#endif /* REPORTTASK_INC_REPORTTASK_H_ */
