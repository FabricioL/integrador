
#include "esp_system.h"
#include "esp_event.h"
#include "esp_event_loop.h"
#include "nvs_flash.h"
#include "driver/gpio.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"

#include "lwip/sockets.h"
#include "lwip/dns.h"
#include "lwip/netdb.h"

#include "../Temperature/inc/temperature.h"
#include "../ReportTask/inc/reportTask.h"

int app_main(void)
{
	// Create the report task
	report_init();

	BaseType_t stat;

	// TODO: unify task creation
    stat = xTaskCreate(readTemp, (const char*) "readTemp", configMINIMAL_STACK_SIZE*5, NULL, 1, NULL);
    if (stat != pdPASS)
    {
    	printf("cannot create readTemp task\n");
    	return 0;
    }

    return 1;
}




