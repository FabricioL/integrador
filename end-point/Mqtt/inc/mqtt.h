/*
 * mqtt.h
 *
 *  Created on: 1 sep. 2021
 *      Author: ischneider
 */

#ifndef MQTT_INC_MQTT_H_
#define MQTT_INC_MQTT_H_

#include "mqtt_client.h"

typedef enum mqtt_state_t_{
	MQTT_CONNECTED,
	MQTT_DISCONNECTED,
}mqtt_state_t;

esp_mqtt_client_handle_t client;

xSemaphoreHandle s_semph_get_mqtt_broker;

void mqtt_app_start(void);
void mqtt_app_end(void);
uint8_t mqtt_publish(char *topic, char *buffer, uint32_t lenth);


#endif /* MQTT_INC_MQTT_H_ */
