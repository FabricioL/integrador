import matplotlib.pyplot as plt
import seaborn as sns

class Resultados:
    def __init__(self, d):
        self.datos = d

    def grafResultados(self):
        fig, ax = plt.subplots(figsize=(20, 2))
        ax.plot(self.datos.index, self.datos.Temp.values)
        plt.show()

    def grafBoxPlots(self):
        sns.boxplot(x="Id", y="Temp", data=self.datos)
        plt.show()
