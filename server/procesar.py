from procesamiento.Generador import Generador
from procesamiento.Recolector import *
from procesamiento.Procesador import *
from recoleccion.Estadisticas import *
from presentacion.Resultados import *


if __name__ == '__main__':
    # Levantar del archivo o de la nube
    ra = Recolector_Archivo()
    lista_valores = ra.leer("Guardar2.csv")

    proc = Procesador()
    proc.agregarListaCadenas(lista_valores)
    pdLista = proc.getPDListaCadenas()

    # Calcular estadisticas
    # selpd = pdLista[pdLista['Id'] == '3']  # Para un nodo
    # est = Estadisticas(selpd['Temp'])
    # print(est.getEstadisticas())
    # est = Estadisticas(pdLista)# Para todos los nodos
    # print(est.getEstadisticas())

    est = Estadisticas(pdLista)
    ## Menos y más temperatura
    est.getNodoMaxMinTemp()

    ## Menos y más registros
    est.getNodoMaxMinReg()

    ## Menos y más antiguo
    est.getNodoMaxMinAntiguo()


    # Presentación de los datos
    ## Grafico de lineas sobre los valores de un sensor
    #pres = Resultados(selpd)
    #pres.grafResultados()


    ## Boxplots de todos las mediciones de todos los sensores
    pres = Resultados(pdLista)
    pres.grafBoxPlots()


