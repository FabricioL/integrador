
class Medicion:
    """
        ...
    Attributes
    ----------
    t : float
        Un numero flotante que guarda la temperatura de una sola medicion
    d : str
        String que guarda la fecha en la que se produjo la medicion
    sensor : str
        String que guarda la id del sensor que tomo la medicion

    """
    def __init__(self, t, d, sensor):
        self.temp = t
        self.d = d
        self.id_sensor = sensor

    def setTemp(self, t):
        self.temp = t

    def setFec(self, d):
        self.d =d

    def setIdSensor(self, sensor):
        self.id_sensor = sensor

    def getTemp(self):
        return self.temp

    def getFec(self):
        return self.d

    def getIdSensor(self):
        return self.id_sensor

    def __str__(self):
        s = "Id={idd} Te={mm} F={ff}\n".format(idd=self.id_sensor,mm = self.temp,ff= self.d)
        return s

