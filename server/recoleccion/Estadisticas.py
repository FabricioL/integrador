class Estadisticas:
    """
        Clase dedicada para extraer estadisticas desde un dataframe de pandas
    """

    def __init__(self, d):
        self.datos = d

    def getEstadisticas(self):
        return self.datos.describe()

    def getNodoMaxMinTemp(self):
        temp_max = max(self.datos['Temp'])
        id_temp_max = self.datos.loc[self.datos['Temp'] == temp_max]['Id']
        id_temp_max = id_temp_max.values.item()
        temp_min = min(self.datos['Temp'])
        id_temp_min = min(self.datos.loc[self.datos['Temp'] == temp_min]['Id'])

        print("El nodo que temperatura mas alta tiene es {idmx} con {tmx}".format(idmx=id_temp_max,tmx=temp_max))
        print("El nodo que temperatura mas bajas tiene es {idmx} con {tmx} ".format(idmx=id_temp_min,tmx=temp_min))

    def getNodoMaxMinReg(self):
        bbb = self.datos.groupby(['Id']).size()
        id_reg_max = bbb.index[bbb == max(bbb)].item()
        id_reg_min = bbb.index[bbb == min(bbb)].item()
        print("El nodo que tiene mas registos es {idmx} con {tmx}".format(idmx=id_reg_max, tmx=max(bbb)))
        print("El nodo que tiene menos registros es {idmx} con {tmx} ".format(idmx=id_reg_min, tmx=min(bbb)))


    def getNodoMaxMinAntiguo(self):
        time_max = min(self.datos['Fecha'])
        id_time_max = self.datos.loc[self.datos['Fecha'] == time_max]['Id'].item()
        time_min = max(self.datos['Fecha'])
        id_time_min = self.datos.loc[self.datos['Fecha'] == time_min]['Id'].item()
        print("El nodo antiguo es: {idmx} su primer registro:{tmx}".format(idmx=id_time_max,tmx=time_max))
        print("El nodo nuevo es : {idmx} su primer registro:{tmx}".format(idmx=id_time_min,tmx=time_min))