import random
import os
import numpy as np
import time

class Generador:
    """
     Clase usada para generar aleatoriamenteprocesar de las mediciones
       ...
       Methods
       -------
       generar_archivo(filename, Num)
           Crea en un archivo con mediciones generadas aleatoriamente


    """

    def __init__(self):
        if not os.path.exists("valores"):
            os.makedirs("valores")

    def str_time_prop(start, end, time_format, prop):

        stime = time.mktime(time.strptime(start, time_format))
        etime = time.mktime(time.strptime(end, time_format))

        ptime = stime + prop * (etime - stime)

        return time.strftime(time_format, time.localtime(ptime))

    def random_date(self,start, end, prop):
        return self.str_time_prop(start, end, '%m/%d/%Y %I:%M %p', prop)

    def generar_archivo(self,filename, Num):
        """
           Parameters
           ----------
            filename : str
               Nombre del archivo donde se va a guardar los datos generados
               en la carpeta valores
            Num : int
                Cantidad de mediciones generadas aleatoriamente


           """
        save_path = 'valores'

        completename = os.path.join(save_path, filename)

        f1 = open(completename,"w+")

        temps = np.random.normal(20, 10, Num)
        ids = np.random.uniform(0, 10, Num)
        for i in range(0,Num):
            f1.write("{id},{N},{fc},\n".format(id=ids[i],N=temps[i],fc= self.random_date("1/1/2021 1:30 PM", "1/1/2022 1:00 AM", random.random())))
        f1.close()
