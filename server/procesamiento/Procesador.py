from recoleccion.Medicion import Medicion
import pandas as pd
from random import seed
import os
import re
import json



class Procesador:
    """
       Clase usada para procesar de las mediciones
       ...
       Attributes
       ----------
       lista_mediciones : Dictionary
           Un diccionario que contiene todas las mediciones identificadas por un entero
       clave : int
           Valor autoincremental que sirve para identificar cada medicion en el conjunto

       Methods
       -------
       agregarMed(Medicion)
           Agrega una medicion a la estructura

        agregarMed(ll)
            Dada una lista de strings cada strings es formateado a una medicion y es agregado a la estructura

        getListaCadenas()
            Devuelve el diccionario con las mediciones que se han agregado

        getPDListaCadenas()
            Devuelve una dataFrame de pandas con el diccionario con las mediciones que se han agregado

        guardarListaCadenas(nombre_archivo)
            Guarda en un archivo (nombre_archivo) en la carpeta valores/ el diccionario
        parseMed(cadena)
            Transforma un string con formato JSON en una Medición id,temp,fecha
       """
    def __init__(self):
        self.lista_mediciones = {}
        self.clave = 0

    def __str__(self):
        s = "Valores: ******\n"
        for k, v in self.lista_mediciones.items():
            s += "Clave: {kk} Valor {vv}".format(kk=k, vv=v)
        return s

    def agregarMed(self,m):
        """
        Parameters
        ----------
        m : Medicion
            Agregar una medicion al diccionario

        """
        self.lista_mediciones[str(self.clave)] = m
        self.clave += 1

    def agregarListaCadenas(self,ll):
        """
        Parameters
        ----------
        ll : list of strings
            Lista de string donde cada string contiene una medicion
        """
        seed(1)
        for i in ll:
            i = i.rstrip(",\n") #'123, 24.2, 1/9/2021 15:46:58 '
            matches = re.findall(r"[a-zA-Z0-9\":\s \./]+,", i)
            if matches:
                matches = [s.replace(',', '') for s in matches]
                m = Medicion(matches[1],matches[2], matches[0])
                self.lista_mediciones[str(self.clave)] = m
                self.clave +=1

    def getListaCadenas(self):
        """
        Returns
        -------
        dict
            Un diccionario con cada medicion insertada
        """
        return self.lista_mediciones


    def getPDListaCadenas(self):
        """
        Returns
        -------
        dataframe
            Un pandas dataframe con tres columnas Id, Temp y Fecha
        """
        ltemp = []
        lfecha = []
        lid = []
        for k, v in self.lista_mediciones.items():
            ltemp.append(float(v.getTemp()))
            lfecha.append(str(v.getFec()))
            lid.append(str(v.getIdSensor()))
        data = {'Id':lid, 'Temp': ltemp, 'Fecha': lfecha}
        return pd.DataFrame.from_dict(data)

    def guardarListaCadenas(self,nombre_archivo):
        """
        Parameters
        ----------
        nombre_archivo : str
            Nombre del archivo donde se va a guardar
        """
        completename = os.path.join("valores", nombre_archivo)
        with open(completename, 'a+') as csvfile:
            for k, v in self.lista_mediciones.items():
                if(v != 0):
                    csvfile.write("{idd},{temp},{fecha}, \n".format(idd = v.getIdSensor(),temp = v.getTemp(), fecha = v.getFec()))

    def guardarMedicion(self,nombre_archivo):
        """
        Parameters
        ----------
        nombre_archivo : str
            Nombre del archivo donde se va a guardar una medicion
        """
        completename = os.path.join("valores", nombre_archivo)
        with open(completename, 'a+') as csvfile:
            v = self.lista_mediciones[str(self.clave-1)]
            if v is not None:
                csvfile.write("{idd},{temp},{fecha}, \n".format(idd = v.getIdSensor(),temp = v.getTemp(), fecha = v.getFec()))


    @classmethod
    def parseMedicion(self, cadena):

        matches = re.finditer(r"(?:{...*})", cadena, re.MULTILINE)

        for matchNum, match in enumerate(matches, start=1):

            print("Match {matchNum} was found at {start}-{end}: {match}".format(matchNum=matchNum,
                                                                                start=match.start(),
                                                                                end=match.end(),
                                                                                match=match.group()))
        match = match.group()

        matches = re.finditer(r":\s[^,|}]*", match, re.MULTILINE)


        for matchNum, match in enumerate(matches, start=1):
            ss = match.group()
            ss = ss[1:]
            if (matchNum == 1):
                print("{m}".format(m=ss))
                id= ss
            if (matchNum == 2):
                print("{m}".format(m=ss))
                temp = float(ss)
            if (matchNum == 3):
                print("{m}".format(m=ss))
                date = ss


        print("ID: {id} TEMP: {temp} DATE: {date} \n".format(id=id,temp=temp,date=date))
        med = Medicion(temp,date,id)
        return med

    @classmethod
    def parseMed(self, cadena):
        """
            Parameters
            ----------
            cadena : str
                Cadena en formato JSON que se convertirá en una instancia de Medicion
        """
        matches = re.search(r"{([a-zA-Z0-9\": ,\./]+)}", cadena)
        if matches:
            y = json.loads(matches.group(0))
            med = Medicion(y["temp"], y["timestamp"], y["Id"])
            return med