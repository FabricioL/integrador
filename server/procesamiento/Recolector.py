import paho.mqtt.client as mqtt
import procesamiento.Procesador as pp


class Recolector:
    """
    Clase abstracta
    establece el formato de entrada de los datos
    cada implementacion es un forma de ingresar los datos
    """
    def __init__(self, *args, **kwargs):
        pass
    def leer(self, value):
        raise NotImplemented

class Recolector_Archivo(Recolector):
    """
    Clase que implementa la entrada de datos desde un archivo con formato csv
    Methods
       -------
       leer( client, value)
       Dado el nombre de un archivo (value) devuelve una lista con todas las lineas del archivo
       en formato string
    """
    def __init__(self, *args, **kwargs):
        pass

    def leer(self, value):
        f1 = open("valores/"+value, "r")
        Lineas = f1.readlines()
        f1.close()
        return Lineas 

class Recolector_MQTT(Recolector):
    """
    Clase que implementa la entrada de datos desde un servidor MQTT
     Methods
       -------
       leer( client, userdata, msg)
            Lee desde el broker una medicion, la convierte a una instancia de Medicion
            la guarda en un diccionario y esa medicion se vuelca en un archivo

    """
    def __init__(self,  ip, port,archivo):
        self.ip = ip
        self.port = port
        self.client = mqtt.Client()
        self.topic = ""
        self.proc = pp.Procesador()
        self.archivo = archivo

    def connect(self):
        self.client.on_connect = self.on_connect
        self.client.on_message = self.leer
        self.client.connect(self.ip, self.port, 60)
        self.client.loop_start()

    def subscribe(self, topic):
        if (self.client.is_connected()):
            self.client.subscribe(topic)
        else:
            self.topic = topic

    def on_connect(self, client, userdata, flags, rc):
        if (self.topic != ""):
            self.client.subscribe(self.topic)

    def leer(self, client, userdata, msg):
        self.proc.agregarMed(pp.Procesador.parseMed(str(msg.payload)))
        self.proc.guardarMedicion(self.archivo)
        print(msg.topic + " " + str(msg.payload))

    def disconnect(self):
        self.client.loop_stop()
        self.client.disconnect()

    def __del__(self):
        self.client.loop_stop()

