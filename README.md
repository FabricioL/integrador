# Repositorio correspondiente al TP Integrador CURSO 2021
# Capacitación Semillero DISW

Este repositorio implementa una comunicación entre distintos dispositivos 
que capturan valores de temperatura junto con un aplicación encargada de mostrar los datos y extraer algunas estadisticas 

En el siguiente diagrama se muestra el estructura del código 
![image](preset/img/TP.png)

En la raiz del repositorio hay 3 carpetas: 

1. 'end-point': para la extraccion de los datos desde los sensores y publicación en el broker
1. 'server': para la recepción de los datos desde el broker y visualización de los mismo
1. 'preset': para diapositivas donde se encuentra una explicación más detallada del código